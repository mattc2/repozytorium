const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should say my name', done => {
    agent
      .get('/api')
      .query({ q: 'fc561f30: what is your name' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Mattc')
        done()
      })
  })

  it('should return highest number', done => {
    agent
      .get('/api')
      .query({ q: '64a8d230: which of the following numbers is the largest: 25, 634, 638, 72' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('638')
        done()
      })
  })

  it('should return sum', done => {
    agent
      .get('/api')
      .query({ q: 'c931f0f0: what is 4 plus 14' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('18')
        done()
      })
  })

  it('should return return of multiplication', done => {
    agent
      .get('/api')
      .query({ q: '7137ea30: what is 9 multiplied by 7' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('63')
        done()
      })
  })

  it('should return return of subtraction', done => {
    agent
      .get('/api')
      .query({ q: '7137ea30: what is 16 minus 13' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('3')
        done()
      })
  })

  it('should return Sean Connery', done => {
    agent
      .get('/api')
      .query({ q: 'b1d35250: who played James Bond in the film Dr No' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Sean Connery')
        done()
      })
  })

  it('should return Paris', done => {
    agent
      .get('/api')
      .query({ q: '6a6f6e50: which city is the Eiffel tower in' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Paris')
        done()
      })
  })

  it('should return 9th number from Fibonacci sequence', done => {
    agent
      .get('/api')
      .query({ q: '5558cf70: what is the 9th number in the Fibonacci sequence' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('34')
        done()
      })
  })

  it('should return colour of banana', done => {
    agent
      .get('/api')
      .query({ q: '0a131240: what colour is a banana' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('yellow')
        done()
      })
  })

  //   95d6b510: what is 19 plus 8 multiplied by 6
  it('should return correct result', done => {
    agent
      .get('/api')
      .query({ q: '95d6b510: what is 19 plus 8 multiplied by 6' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('67')
        done()
      })
  })

  it('should return correct result', done => {
    agent
      .get('/api')
      .query({ q: 'f8adbf60: what is 14 plus 1 plus 15' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('30')
        done()
      })
  })

  it('should return correct result', done => {
    agent
      .get('/api')
      .query({ q: '95d6b510: what is 19 multiplied by 8 plus 6' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('158')
        done()
      })
  })

  it('should return square and a cube number', done => {
    agent
      .get('/api')
      .query({ q: '2e445af0: which of the following numbers is both a square and a cube: 81, 313, 873, 4096' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('4096')
        done()
      })
  })

  it('should return prime number', done => {
    agent
      .get('/api')
      .query({ q: 'f99af270: which of the following numbers are primes: 74, 31, 895, 487' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('31')
        done()
      })
  })

  it('should return result of 12 to the power of 15', done => {
    agent
      .get('/api')
      .query({ q: '0a131240: what is 12 to the power of 15' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('15407021574586368')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
