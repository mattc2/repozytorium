#!/bin/sh

# Build the image
docker image build -f Dockerfile -t geeqla/$HEROKU_APP_NAME:latest .

# Push to Heroku
docker tag geeqla/$HEROKU_APP_NAME:latest registry.heroku.com/$HEROKU_APP_NAME/web
echo "$HEROKU_API_KEY" | docker login -u _ --password-stdin registry.heroku.com
docker push registry.heroku.com/$HEROKU_APP_NAME/web

# Call Herokup API to release the image
docker inspect registry.heroku.com/$HEROKU_APP_NAME/web --format={{.Id}} > WEB_DOCKER_IMAGE_ID
export WEB_DOCKER_IMAGE_ID=$(cat WEB_DOCKER_IMAGE_ID)
curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME/formation \
-d '{
"updates": [
    {
    "type": "web",
    "docker_image": "'"$WEB_DOCKER_IMAGE_ID"'"
    }
]
}' \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_API_KEY"