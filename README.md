# Hackathon game app

[![codecov](https://codecov.io/bb/mattc2/repozytorium/branch/master/graph/badge.svg)](https://codecov.io/gh/mattc2/hackathon)

This template expands functionality of the [base application](https://github.com/geeqla/base-express/tree/base) with a basic API backend and Continuous Integration (CI) support.

## Commands

### `yarn dev`

Starts dev nodemon on [http://localhost:3000](http://localhost:3000)

### `yarn test`

Launches mocha test runner

### `yarn codecov`

Launches mocha test runner + displays code coverage report + uploads code coverage report to [codecov.io](https://codecov.io)

### `yarn start`

Starts the app in a cluster mode.
