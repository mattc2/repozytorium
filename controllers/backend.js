module.exports = {
  hackathon: (req, res) => {
    const query = req.query
    const question = query.q
    let answer = ''

    function fibonacci(n) {
      let arr = [0, 1]
      for (let i = 2; i < n + 1; i++) {
        arr.push(arr[i - 2] + arr[i - 1])
      }
      return arr[n]
    }

    function isPrime(num) {
      for (var i = 2; i < num; i++) if (num % i === 0) return false
      return num > 1
    }

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console
    if (/what is your name/.test(question)) {
      answer = 'Mattc'
    } else if (/plus \d+ multiplied/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech
        .split(' ')
        .map(a => parseInt(a))
        .filter(b => !!b)
      answer = parts[0] + parts[1] * parts[2]
    } else if (/plus \d+ plus/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech
        .split(' ')
        .map(a => parseInt(a))
        .filter(b => !!b)
      answer = parts[0] + parts[1] + parts[2]
    } else if (/multiplied by \d+ plus/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech
        .split(' ')
        .map(a => parseInt(a))
        .filter(b => !!b)
      answer = parts[0] * parts[1] + parts[2]
    } else if (/which of the following numbers is the largest/.test(question)) {
      const parts = question.split(':')
      const numbers = parts[2]
        .trim()
        .split(',')
        .map(number => parseInt(number.trim()))
      answer = Math.max(...numbers)
    } else if (/plus/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech.split(' ')
      answer = parts.reduce((acc, curr) => {
        const int = parseInt(curr)
        return acc + (int || 0)
      }, 0)
    } else if (/minus/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech
        .split(' ')
        .map(a => parseInt(a))
        .filter(b => !!b)
      answer = parts[0] - parts[1]
    } else if (/multiplied/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech.split(' ')
      const mech = parts.reduce((acc, curr) => {
        const number = parseInt(curr)
        const result = Math.abs(number) > 0 ? acc * number : acc
        return result
      }, 1)
      answer = mech
    } else if (/who played James Bond in the film Dr No/.test(question)) {
      answer = 'Sean Connery'
    } else if (/Fibonacci sequence/.test(question)) {
      const parts = question.split(':')
      const number = parts[1]
        .split(' ')
        .map(a => parseInt(a))
        .filter(a => !!a)
      answer = fibonacci(number)
    } else if (/which city is the Eiffel tower in/.test(question)) {
      answer = 'Paris'
    } else if (/what colour is a banana/.test(question)) {
      answer = 'yellow'
    } else if (/power/.test(question)) {
      const ech = question.split(':')[1].trim()
      const parts = ech
        .split(' ')
        .map(a => parseInt(a))
        .filter(b => !!b)
      answer = Math.pow(parts[0], parts[1])
    } else if (/which of the following numbers are primes/.test(question)) {
      const parts = question.split(':')
      const numbers = parts[2]
        .trim()
        .split(',')
        .map(a => parseInt(a.trim()))
        .filter(b => !!isPrime(b))
      answer = numbers[0]
    } else if (/which of the following numbers is both a square and a cube/.test(question)) {
      const parts = question.split(':')
      const numbers = parts[2]
        .trim()
        .split(',')
        .map(a => parseInt(a.trim()))
        .filter(b => Number.isInteger(Math.cbrt(b)))
      answer = numbers[0]
    } else {
      answer = question || 'Hello Hackathon'
    }
    // f99af270: which of the following numbers are primes: 74, 31, 895, 487
    // Answer the question
    res.status(200).send(String(answer))
  }
}
